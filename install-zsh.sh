#!/usr/bin/env zsh
PROJECT_NAME="AUTO_SWITCH_ALIAS"

TAG="###${PROJECT_NAME}###ADDON ZSHRC###"
TARGET_ZSHRC_PROMPT_OVERWRITE="
${TAG}
PROMPT_COMMAND='[[ -f .autoaliases.sh ]] && source .autoaliases.sh;'
precmd() { eval "$PROMPT_COMMAND" }
${TAG}
"
test=$(grep  "${TAG}" ~/.zshrc|wc -l)

if [ $test -eq 0 ]; then
    echo "${TARGET_ZSHRC_PROMPT_OVERWRITE}" >> ~/.zshrc
else
    echo 'already installed'
fi
