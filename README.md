# AutoSwitchAlias

## Description

A cool stuff to use the good local commands in the good environment for development of a Web Project.

### History 
This project was created after work under differents projects using différents versions of dev-shell-commands like composer, npm , yarn, gulp, console (symfony) 

In fact, with composer or others tools a developer need to run install and configure commands  in a specific environnment.

For example : 
He can't use his local composer, and need to create and use the good environment to generate and develop in the same condition like the delivery envirionment (rec,staging, production,..etc)
His desktop can use PHP 7.1 but the production of Project 1 can use PHP 5.5 and a test project can use PHP 7.2.

It is now usual to use a Docker environnment for that. But it requires to use a very long docker command to make the expected command 

```bash
$  docker run -it --rm -v $(pwd):/var/www -u 1000:www-data  ppottie/c-pyng:php7.2 bin/console install

```
 
Furthermore, if we change for an other project, this command must be change and we must adapt it for this other project.
 
```bash
$  docker run -it --rm -v $(pwd):/var/www/app -u 1000:nginx  ppottie/c-pyng:php5.4 bin/console install

```


This Project want to make easly to use specific common commands for each projects.
 
## INSTALL
just run install.sh 

## HOW TO USE

create in *YOUR project* a  script file ``.autoaliases.sh``

A complex example of  ``.autoaliases.sh``:
```bash
#!/bin/sh

function dotenv () {
  set -a
  [ -f .env ] && . .env
  export uid=$(stat -c %u .)
  export gid=$(stat -c %g .)
  dockercomposeYML=""
  if [[  -f docker-compose.dist.yml ]]; then
    dockercomposeYML=" -f docker-compose.dist.yml"
  fi
  if [[  -f docker-compose.${APP_ENV}.yml ]]; then
    dockercomposeYML="${dockercomposeYML} -f docker-compose.${APP_ENV}.yml"
  fi
  if [[ -f docker-compose.yml ]] || [[ $dockercomposeYML ]]; then
    export DC="docker-compose ${dockercomposeYML} exec -u ${uid}:${gid}"
  fi
  set +a
}


function composer () {
   dotenv
   if [ $(pwd) == ${PROJECT_PATH} ] && [[ $DC ]]; then
       eval ${DC} api-php-fpm composer $@
   fi
}

function console () {
   dotenv
   if [ $(pwd) == ${PROJECT_PATH} ] && [[ $DC ]]; then
       eval ${DC} api-php-fpm bin/console $@
   fi
}
function behat () {
   dotenv
   if [ $(pwd) == ${PROJECT_PATH} ] && [[ $DC ]]; then
       eval ${DC} api-php-fpm vendor/bin/behat $@
   fi
}

function yarn () {
   dotenv
   if [ $(pwd) == ${PROJECT_PATH} ] && [ -f docker-compose.${APP_ENV}.yml ]; then
        docker pull ppottie/c-pyng:php7.1
        docker run -it --rm -v $(pwd):/var/www -u  ${uid}:${gid}  ppottie/c-pyng:php7.1  yarn $@
   fi
}
function gulp () {
    dotenv
   if [ $(pwd) == ${PROJECT_PATH} ] && [ -f docker-compose.${APP_ENV}.yml ]; then
    docker pull ppottie/c-pyng:php7.1
       docker run -it --rm -v $(pwd)/var/.yarnrc:/.yarnrc -v $(pwd):/var/www -u  ${uid}:${gid} ppottie/c-pyng:php7.1 gulp $@
   else
        gulp $@
   fi
}



```


Now , you start in a new console or run ```source ~/.bashrc```

When you enter in a directory (your project) containing ``.autoaliases.sh``, this script will loaded.

So If this script file contents overwrite commands like in example , composer or other command will use local command
  
## EXAMPLE
 
Install the project and try this :
 
run ```composer -V``` in ProjectA directory's

```bash
$ cd example/projectA
$ composer -V
Composer version 1.6.2 2018-01-05 15:28:41
```
run ```composer -V``` in ProjectB directory's
```bash
$ cd ../projectB
$ composer -V
Composer version 1.5.5 2017-12-01 14:42:57

```
